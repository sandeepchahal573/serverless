import path from "path";
import fs from "fs";
import getNewPage from "../../config/playWright";

export default async (req, res) => {
  console.log("got the request");
  const t1 = Date.now();
	const page = await getNewPage();
	try {
		const pathToFile = path.join("public", "example.png");

		// const page = await browser.newPage();
		await page.goto(req.query.url);
		await page.screenshot({
			fullPage: true,
			path: pathToFile,
		});
		await page.close();
		const readStream = fs.createReadStream(pathToFile);

		readStream.pipe(res);
	} catch (err) {
		console.log(err);
		res.json(err.message);
	}
  const t2 = Date.now();
  console.log("Finished in "+((t2-t1)/1000)+" seconds");
};
