import { useState } from "react";
import { useRouter } from "next/router";

export default function Home() {
	const [value, setValue] = useState("");
	const router = useRouter();
	return (
		<div
			style={{
				display: "flex",
				flexDirection: "column",
				justifyContent: "center",
				alignContent: "center",
			}}
		>
			<input
				style={{ margin: "20px auto", width: "400px", padding: "10px" }}
				type="text"
				placeholder="Enter a url"
				value={value}
				onChange={(e) => setValue(e.target.value)}
			/>
			<button
				style={{ margin: "20px auto", width: "200px" }}
				onClick={() => router.push(`/api?url=${value}`)}
			>
				Take Screenshot
			</button>
		</div>
	);
}
