import puppeteer from "puppeteer";
import path from "path";

console.log(path.join("..", "chrome-win", "chrome.exe"));

let browser = null;

export default async () => {
	if (browser && browser.isConnected()) return browser.newPage();
	try {
		browser.disconnect();
	} catch (err) {}
	console.log("opening new browser");

//	const browserFetcher = puppeteer.createBrowserFetcher({
		// platform: "linux",
//		path: path.resolve("./"),
//	});
//	const revisionInfo = await browserFetcher.download("818858");
//	console.log(revisionInfo)
	console.log(path.resolve(".","linux-818858","chrome-linux","chrome"))
	browser = await puppeteer.launch({
		headless: true,
		executablePath: path.resolve(".","linux-818858","chrome-linux","chrome"),
	});
	return browser.newPage();
};
